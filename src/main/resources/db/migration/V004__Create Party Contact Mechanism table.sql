CREATE TABLE `contact_mechanism` (
  `uuid` varchar(255) NOT NULL,
  `type_uuid` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `class` varchar(255) NOT NULL,
  PRIMARY KEY (`uuid`),
  KEY `FK_term` (`type_uuid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE `party_contact_mechanism` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `party_uuid` varchar(255) NOT NULL,
  `contact_mechanism_uuid` varchar(255) NOT NULL,
  `restricted` bit(1) NOT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `from_date` datetime DEFAULT NULL,
  `thru_date` datetime DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `last_updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_contact_mechanism` (`contact_mechanism_uuid`),
  KEY `FK_party` (`party_uuid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;
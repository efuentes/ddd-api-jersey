CREATE TABLE `party_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `party_uuid` varchar(255) NOT NULL,
  `type_uuid` varchar(255) NOT NULL,
  `restricted` bit(1) NOT NULL,
  `from_date` datetime DEFAULT NULL,
  `thru_date` datetime DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `last_updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_party` (`party_uuid`),
  KEY `FK_term` (`type_uuid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

ALTER TABLE `party_role`
  ADD CONSTRAINT `FK_term` FOREIGN KEY (`type_uuid`) REFERENCES `term` (`uuid`),
  ADD CONSTRAINT `FK_party` FOREIGN KEY (`party_uuid`) REFERENCES `party` (`uuid`);
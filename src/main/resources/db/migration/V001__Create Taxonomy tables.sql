CREATE TABLE `vocabulary` (
  `uuid` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `last_updated` datetime DEFAULT NULL,
  `restricted` bit(1) NOT NULL,
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE `term` (
  `uuid` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `restricted` bit(1) NOT NULL,
  `vocabulary_uuid` varchar(255) NOT NULL,
  `parent_uuid` varchar(255) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `last_updated` datetime DEFAULT NULL,
  PRIMARY KEY (`uuid`),
  KEY `FK_parent_term` (`parent_uuid`),
  KEY `FK_vocabulary` (`vocabulary_uuid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

ALTER TABLE `term`
  ADD CONSTRAINT `FK_parent_term` FOREIGN KEY (`parent_uuid`) REFERENCES `term` (`uuid`),
  ADD CONSTRAINT `FK_vocabulary` FOREIGN KEY (`vocabulary_uuid`) REFERENCES `vocabulary` (`uuid`);
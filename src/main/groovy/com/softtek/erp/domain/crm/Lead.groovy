package com.softtek.erp.domain.crm

import com.softtek.erp.domain.taxonomy.Vocabulary
import com.softtek.erp.domain.taxonomy.Term
import com.softtek.erp.domain.party.Person
import com.softtek.erp.domain.party.Organization
import com.softtek.erp.domain.party.PhoneNumber
import com.softtek.erp.domain.party.Email
import com.softtek.erp.domain.party.PostalAddress
import com.softtek.erp.domain.party.PartyRelationship

class Lead {

  static class Builder {
    private Person person
    private PartyRelationship partyRelationship
    private Organization company

    def Builder(String firstName, String lastName) {
      Vocabulary partyRoleVocabulary = new Vocabulary("PARTY_ROLE")
      Term personRoleTerm = new Term("PERSON_ROLE", partyRoleVocabulary)
      Term leadRoleTerm = new Term("LEAD" ,personRoleTerm)
      Term employeeRoleTerm = new Term("EMPLOYEE", personRoleTerm)

      this.person = new Person(firstName, lastName)
        .withPartyRole(leadRoleTerm)
        .withPartyRole(employeeRoleTerm)
    }

    Lead build() {
      return new Lead(this)
    }

    Builder withPersonalEmail(String email) {
      Vocabulary contactMechanismPurposeTypeVocabulary = new Vocabulary("CONTACT_MECHANISM_PURPOSE_TYPE")
      Term emailPurposeTerm = new Term("EMAIL_PURPOSE_TYPE", contactMechanismPurposeTypeVocabulary)
      Term personalEmail = new Term("PERSONAL_EMAIL", emailPurposeTerm)

      Vocabulary contactMechanismTypeVocabulary = new Vocabulary("CONTACT_MECHANISM_TYPE")
      Term emailTerm = new Term("EMAIL_TYPE", contactMechanismTypeVocabulary)
      Term personalTerm = new Term("PERSONAL", emailTerm)
      Email emailAddress = new Email(email, personalTerm)

      this.person.withContactMechanism(emailAddress, personalEmail)

      return this
    }

    Builder withPostalAddress(PostalAddress postalAddress) {
      this.person.withPostalAddress(postalAddress)

      return this
    }

    Builder withOrganization(String name) {
      Vocabulary partyRoleVocabulary = new Vocabulary("PARTY_ROLE")
      Term organizationRoleTerm = new Term("ORGANIZATION_ROLE", partyRoleVocabulary)
      Term organizationUnitRoleTerm = new Term("INTERNAL_ORGANIZATION", organizationRoleTerm)

      def company = new Organization(name)
        .withPartyRole(organizationUnitRoleTerm)

      this.company = company

      Vocabulary partyRelationshipVocabulary = new Vocabulary("PARTY_RELATIONSHIP_TYPE")
      Term partyRelationshipTerm = new Term("EMPLOYMENT", partyRelationshipVocabulary)

      def partyRelationship = new PartyRelationship(company.partyRoles[0], this.person.partyRoles[0], partyRelationshipTerm)

      this.partyRelationship = partyRelationship

      return this
    }
  }

  private final Person person
  private final PartyRelationship partyRelationship
  private final Organization company

  def Lead(Builder builder) {
    this.person = builder.person
    this.partyRelationship = builder.partyRelationship
    this.company = builder.company
  }
}
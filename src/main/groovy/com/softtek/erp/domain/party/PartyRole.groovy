package com.softtek.erp.domain.party

import com.softtek.erp.domain.taxonomy.Term

class PartyRole {

  Party party
  Term type

  def PartyRole(Party party, Term type) {
    this.party = party
    this.type = type
  }
}
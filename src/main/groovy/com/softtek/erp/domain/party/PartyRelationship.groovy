package com.softtek.erp.domain.party

import com.softtek.erp.domain.taxonomy.Term

class PartyRelationship {

  PartyRole from
  PartyRole to
  Term type

  def PartyRelationship(PartyRole from, PartyRole to, Term type) {
    this.from = from
    this.to = to
    this.type = type
  }
}
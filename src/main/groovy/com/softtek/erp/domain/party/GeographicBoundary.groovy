package com.softtek.erp.domain.party

import com.softtek.erp.domain.taxonomy.Term

class GeographicBoundary {

  String code
  Term type
  String name
  String abbreviation
  GeographicBoundary parent

  def GeographicBoundary(String code, Term type) {

    if (type.vocabulary.code != 'GEOGRAPHIC_BOUNDARY') {
      throw new IllegalArgumentException("Geographic Boundary Term must belong to Vocabulary 'GEOGRAPHIC_BOUNDARY'.")
    }

    this.code = code
    this.type = type
  }

  GeographicBoundary withName(String name) {
    this.name = name

    return this
  }

  GeographicBoundary withAbbreviation(String abbreviation) {
    this.abbreviation = abbreviation

    return this
  }

  GeographicBoundary withParent(GeographicBoundary parent) {
    this.parent = parent

    return this
  }

}
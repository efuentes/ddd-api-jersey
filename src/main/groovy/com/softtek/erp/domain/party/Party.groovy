package com.softtek.erp.domain.party

import com.softtek.erp.domain.taxonomy.Vocabulary
import com.softtek.erp.domain.taxonomy.Term

import com.softtek.ddd.Entity

abstract class Party extends Entity {

  def partyRoles = []
  def postalAddresses = []
  def contactMechanisms = []

  def Party() {
    super()
  }

  Party withPartyRole(Term partyRoleTerm) {

    if (partyRoleTerm.vocabulary.code != 'PARTY_ROLE') {
      throw new IllegalArgumentException("Party Role Term must belong to Vocabulary 'PARTY_ROLE'.")
    }

    def isDuplicated = false
    partyRoles.each { partyRole ->
      if (partyRole.type.code == partyRoleTerm.code) {
        isDuplicated = true
      }
    }

    if (isDuplicated) {
      throw new IllegalArgumentException("Party Role already added to Party.")
    }

    this.partyRoles.add(new PartyRole(this, partyRoleTerm))

    return this
  }

  Party withPostalAddress(PostalAddress postalAddress) {
    this.postalAddresses.add(postalAddress)

    return this
  }

  Party withContactMechanism(PhoneNumber phone, Term contactMechanismPurposeType) {
    def contactMechanism = [ phone: phone, purpose: contactMechanismPurposeType ]
    this.contactMechanisms.add(contactMechanism)

    return this
  }

  Party withContactMechanism(Email email, Term contactMechanismPurposeType) {
    def contactMechanism = [ email: email, purpose: contactMechanismPurposeType ]
    this.contactMechanisms.add(contactMechanism)

    return this
  }

  Party withContactMechanism(PostalAddress postalAddress, Term contactMechanismPurposeType) {
    def contactMechanism = [ address: postalAddress, purpose: contactMechanismPurposeType ]
    this.contactMechanisms.add(contactMechanism)

    return this
  }
}
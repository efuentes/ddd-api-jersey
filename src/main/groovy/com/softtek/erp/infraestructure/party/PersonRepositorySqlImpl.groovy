package com.softtek.erp.infraestructure.party

import groovy.sql.Sql
import org.joda.time.DateTime

import com.softtek.erp.domain.party.Person

class PersonRepositorySqlImpl implements PersonRepository {

  Sql sql

  def PersonRepositorySqlImpl(Sql sql) {
    this.sql = sql
  }

  @Override
  def findAll() {

    def persons = []

    sql.eachRow("SELECT * FROM party") {
      persons.add(new Person(it.first_name, it.last_name))
    }

    return persons
  }

  @Override
  Person findOneByFirstNameAndLastName(String firstName, String lastName) {

    Person person

    //sql.eachRow("SELECT * FROM person WHERE first_name=${firstName} AND last_name=${lastName} LIMIT 1") {
    //  person = new Person(it.first_name, it.last_name)
    //}

    def row = sql.firstRow("SELECT * FROM party WHERE first_name=${firstName} AND last_name=${lastName}")
    if (row) {
      person = new Person(row.first_name, row.last_name)
      person.uuid = row.uuid
    }

    return person
  }

  @Override
  Person findByUUID(String uuid) {

    Person person

    def row = sql.firstRow("SELECT * FROM party WHERE uuid=${uuid}")
    if (row) {
      person = new Person(row.first_name, row.last_name)
      person.uuid = row.uuid
    }

    return person
  }

  @Override
  void add(Person person) {

    def personTable = sql.dataSet("party")
    personTable.add( uuid: person.uuid,
                     first_name: person.firstName,
                     last_name: person.lastName,
                     date_created: new DateTime().toDate()
                    )
  }
}
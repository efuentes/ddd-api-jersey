package com.softtek.erp.infraestructure.taxonomy

import com.softtek.erp.domain.taxonomy.Vocabulary

interface VocabularyRepository {

  //def findAll()
  Vocabulary findByUUID(String uuid)

  //void add(Lead lead)
}
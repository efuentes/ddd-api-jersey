package com.softtek.erp.infraestructure.taxonomy

import groovy.sql.Sql
import org.joda.time.DateTime

import com.softtek.erp.domain.taxonomy.Vocabulary

class VocabularyRepositorySqlImpl implements VocabularyRepository {

  Sql sql

  def VocabularyRepositorySqlImpl(Sql sql) {
    this.sql = sql
  }

  @Override
  Vocabulary findByUUID(String uuid) {

    Vocabulary vocabulary

    def row = sql.firstRow("SELECT * FROM vocabulary WHERE uuid=${uuid}")
    if (row) {
      vocabulary = new Vocabulary(row.code)
      vocabulary.uuid = row.uuid
    }

    return vocabulary
  }
}
package com.softtek.erp.infraestructure.crm

import groovy.sql.Sql
import org.joda.time.DateTime

import com.softtek.erp.domain.crm.Lead

class LeadRepositorySqlImpl implements LeadRepository {

  Sql sql

  def LeadRepositorySqlImpl(Sql sql) {
    this.sql = sql
  }

  @Override
  def findAll() {

    def leads = []

    def selectLeads = """
    SELECT *
    FROM party_role AS R
      LEFT JOIN party AS P ON P.uuid=R.party_uuid
      LEFT JOIN term AS T ON T.uuid=R.type_uuid
    WHERE T.code='LEAD'
    """

    sql.eachRow(selectLeads) {
      Lead.Builder builder = new Lead.Builder(it.first_name, it.last_name)
      Lead lead = builder.build()
      leads.add(lead)
    }

    return leads
  }
}
package com.softtek.erp.application.api.party.routes

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Path("groovy")
class GroovyRoute {
  @GET
  @Produces(["text/plain"])
  def show() {
    "groovy"
  }
}
package com.softtek.erp.application.api.party.routes

import javax.ws.rs.Path
import javax.ws.rs.GET
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

import com.softtek.erp.application.api.App

import com.softtek.erp.domain.party.Person

import com.softtek.erp.infraestructure.party.PersonRepositorySqlImpl

@Path("persons")
class PersonResource {

  private PersonRepositorySqlImpl personRepository = new PersonRepositorySqlImpl(App.sql)


  @GET
  @Produces(MediaType.APPLICATION_JSON)
  def getAllPersons() {

    def elements = []
    def persons = personRepository.findAll()

    return persons
  }
}
package com.softtek.erp.application.api

import org.glassfish.grizzly.http.server.HttpServer
import org.glassfish.hk2.utilities.binding.AbstractBinder
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory
import org.glassfish.jersey.server.ResourceConfig

import java.net.URI
import java.io.IOException

import groovy.sql.Sql

class App {

  private static final URI BASE_URI = URI.create("http://localhost:8080/api")

  def static sql

  static void main(String[] args) throws IOException {

    final HttpServer server = startServer()

    System.out.println(String.format("Jersey app started with WADL available at " + "%s/application.wadl\n" + "Try out %s/groovy\nHit enter to stop it...", BASE_URI, BASE_URI));

    System.in.read();
    server.shutdownNow();
  }

  static HttpServer startServer() {

    HttpServer server = GrizzlyHttpServerFactory.createHttpServer(BASE_URI, create());

    def db = [url:'jdbc:mysql://127.0.0.1:3306/erp-dev', user:'admin', password:'toor', driver:'com.mysql.jdbc.Driver']
    //def db = [url:'jdbc:mysql://127.0.0.1:49162/erp-test', user:'admin', password:'toor', driver:'com.mysql.jdbc.Driver']
    sql = Sql.newInstance(db.url, db.user, db.password, db.driver)

    return server
  }

  static ResourceConfig create() {
    ResourceConfig resourceConfig = new ResourceConfig()
    resourceConfig.packages("com.softtek.erp.application.api.party")

    return resourceConfig
  }

}
package com.softtek.erp.application.api.party.model

import javax.xml.bind.annotation.XmlRootElement

import com.softtek.erp.domain.party.Person

@XmlRootElement
class PersonResource extends Person {
  
  def PersonElement(Person p) {
    this.uuid = p.uuid
    this.firstName = p.firstName
    this.lastName = p.lastName
    this.gender= p.gender
  }
}
CREATE TABLE IF NOT EXISTS `party` (
  `uuid` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `gender_id` bigint(20) DEFAULT NULL,
  `birth_date` datetime DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `last_updated` datetime DEFAULT NULL,
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `vocabulary` (
  `uuid` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `date_created` datetime NOT NULL,
  `last_updated` datetime NOT NULL,
  `restricted` bit(1) NOT NULL,
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `term` (
  `uuid` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `restricted` bit(1) NOT NULL,
  `vocabulary_uuid` varchar(255) NOT NULL,
  `parent_uuid` varchar(255) DEFAULT NULL,
  `weight` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  `last_updated` datetime NOT NULL,
  PRIMARY KEY (`uuid`),
  KEY `FK_parent_term` (`parent_uuid`),
  KEY `FK_vocabulary` (`vocabulary_uuid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

ALTER TABLE `term`
  ADD CONSTRAINT `FK_parent_term` FOREIGN KEY (`parent_uuid`) REFERENCES `term` (`uuid`),
  ADD CONSTRAINT `FK_vocabulary` FOREIGN KEY (`vocabulary_uuid`) REFERENCES `vocabulary` (`uuid`);

CREATE TABLE IF NOT EXISTS `party_role` (
  `uuid` varchar(255) NOT NULL,
  `party_uuid` varchar(255) NOT NULL,
  `type_uuid` varchar(255) NOT NULL,
  `restricted` bit(1) NOT NULL,
  `from_date` datetime NOT NULL,
  `thru_date` datetime NOT NULL,
  `date_created` datetime NOT NULL,
  `last_updated` datetime NOT NULL,
  PRIMARY KEY (`uuid`),
  KEY `FK_party` (`party_uuid`),
  KEY `FK_term` (`type_uuid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

ALTER TABLE `party_role`
  ADD CONSTRAINT `FK_term` FOREIGN KEY (`type_uuid`) REFERENCES `term` (`uuid`),
  ADD CONSTRAINT `FK_party` FOREIGN KEY (`party_uuid`) REFERENCES `party` (`uuid`);
package com.softtek.erp.infraestructure.crm

import static org.junit.Assert.*
import static org.hamcrest.CoreMatchers.*
import org.junit.*

import org.flywaydb.core.Flyway

import static java.util.UUID.randomUUID
import groovy.sql.Sql

import com.softtek.erp.domain.party.Email
import com.softtek.erp.domain.crm.Lead

class LeadRepositoryTests {

  static Flyway flyway
  static def sql
  
  static def vocabularyTable, termTable
  static def contactMechanismTable
  static def personTable, partyRoleTable, partyContactMechanismTable

  @BeforeClass
  static void setup() {
    def db = [url:'jdbc:mysql://127.0.0.1:3306/erp-dev', user:'admin', password:'toor', driver:'com.mysql.jdbc.Driver']
    //def db = [url:'jdbc:mysql://127.0.0.1:49162/erp-test', user:'admin', password:'toor', driver:'com.mysql.jdbc.Driver']
    sql = Sql.newInstance(db.url, db.user, db.password, db.driver)

    flyway = new Flyway()
    flyway.setDataSource(db.url, db.user, db.password)

    vocabularyTable = sql.dataSet("vocabulary")
    termTable = sql.dataSet("term")
    personTable = sql.dataSet("party")
    partyRoleTable = sql.dataSet("party_role")
    contactMechanismTable = sql.dataSet("contact_mechanism")
    partyContactMechanismTable = sql.dataSet("party_contact_mechanism")
  }

  @AfterClass
  static void teardown() {
    sql.close()
  }

  @Before
  void before() {
    flyway.clean()
    flyway.migrate()
  }

  @After
  void after() {
  }

  @Test
  void canFindAllLeads() {

    // Arrange
    def uuid_lead_term = dbCreateLeadTerm()
    def uuid_email_term = dbCreateEmailTerm()

    def uuid1 = dbCreatePerson("John", "Smith")
    dbCreateLeadFromPerson(uuid1, uuid_lead_term)
    def uuid_cm1 = dbAddEmailToLead(uuid1, "john.smith@mail.com", uuid_email_term)

    def uuid2 = dbCreatePerson("Mary", "Johnson")
    dbCreateLeadFromPerson(uuid2, uuid_lead_term)

    def uuid3 = dbCreatePerson("Laura", "Johanson")


    // Act
    LeadRepositorySqlImpl repository = new LeadRepositorySqlImpl(sql)
    def sut = repository.findAll()

    // Assert
    assertThat sut, notNullValue()
    assertThat sut.size(), is(2)
    assertThat sut[0].person.name(), is("John Smith")
    //assertThat sut[0].person.contact
    assertThat sut[1].person.name(), is("Mary Johnson")
  }

  def dbCreateLeadTerm() {
    def uuid_vocabulary = randomUUID() as String
    vocabularyTable.add( uuid: uuid_vocabulary,
                         code: "PARTY_ROLE",
                         restricted: 1)

    def uuid_term = randomUUID() as String
    termTable.add( uuid: uuid_term,
                   code: "LEAD",
                   vocabulary_uuid: uuid_vocabulary,
                   restricted: 1)

    return uuid_term
  }

  def dbCreateEmailTerm() {
    def uuid_vocabulary2 = randomUUID() as String
    vocabularyTable.add( uuid: uuid_vocabulary2,
                         code: "CONTACT_MECHANISM",
                         restricted: 1)

    def uuid_term = randomUUID() as String
    termTable.add( uuid: uuid_term,
                   code: "EMAIL",
                   vocabulary_uuid: uuid_vocabulary2,
                   restricted: 1)
    return uuid_term
  }

  def dbCreatePerson(String firstName, String lastName) {
    def uuid = randomUUID() as String
    personTable.add( uuid: uuid,
                     first_name: firstName,
                     last_name: lastName
                    )

    return uuid
  }

  def dbCreateLeadFromPerson(String uuid_person, String uuid_lead_term) {
    partyRoleTable.add( party_uuid: uuid_person,
                        type_uuid: uuid_lead_term,
                        restricted: 1)
  }

  def dbAddEmailToLead(String uuid_lead, String email, String uuid_email_term) {
    def uuid_cm = randomUUID() as String
    contactMechanismTable.add( uuid: uuid_cm,
                               type_uuid: uuid_email_term,
                               email: email,
                               class: Email.class)

    partyContactMechanismTable.add( party_uuid: uuid_lead,
                                    contact_mechanism_uuid: uuid_cm,
                                    restricted: 1)

    return uuid_cm
  }
}
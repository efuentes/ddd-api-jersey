package com.softtek.erp.infraestructure.party

import static org.junit.Assert.*
import static org.hamcrest.CoreMatchers.*
import org.junit.*

import org.flywaydb.core.Flyway

import static java.util.UUID.randomUUID
import groovy.sql.Sql

import com.softtek.erp.domain.taxonomy.Vocabulary
import com.softtek.erp.domain.taxonomy.Term
import com.softtek.erp.domain.party.Person
import com.softtek.erp.domain.party.Email

class PersonRepositoryTests {

  static Flyway flyway
  static def sql

  static def vocabularyTable, termTable
  static def contactMechanismTable
  static def personTable, partyRoleTable, partyContactMechanismTable

  @BeforeClass
  static void setup() {
    def db = [url:'jdbc:mysql://127.0.0.1:3306/erp-dev', user:'admin', password:'toor', driver:'com.mysql.jdbc.Driver']
    //def db = [url:'jdbc:mysql://127.0.0.1:49162/erp-test', user:'admin', password:'toor', driver:'com.mysql.jdbc.Driver']
    sql = Sql.newInstance(db.url, db.user, db.password, db.driver)

    flyway = new Flyway()
    flyway.setDataSource(db.url, db.user, db.password)

    vocabularyTable = sql.dataSet("vocabulary")
    termTable = sql.dataSet("term")
    personTable = sql.dataSet("party")
    partyRoleTable = sql.dataSet("party_role")
    contactMechanismTable = sql.dataSet("contact_mechanism")
    partyContactMechanismTable = sql.dataSet("party_contact_mechanism")
  }

  @AfterClass
  static void teardown() {
    sql.close()
  }

  @Before
  void before() {
    flyway.clean()
    flyway.migrate()
  }

  @After
  void after() {
  }

  @Test
  void canFindAllPersons() {

    // Arrange
    def uuid1 = dbCreatePerson("John", "Smith")
    def uuid2 = dbCreatePerson("Mary", "Johnson")

    // Act
    PersonRepositorySqlImpl repository = new PersonRepositorySqlImpl(sql)
    def sut = repository.findAll()

    // Assert
    assertThat sut, notNullValue()
    assertThat sut.size(), is(2)
    //assertThat sut[0].uuid, is(uuid1)
    //assertThat sut[0].name(), is("John Smith")
    //assertThat sut[1].uuid, is(uuid2)
    //assertThat sut[1].name(), is("Mary Johnson")
  }

  @Test
  void canFindPersonByName() {

    // Arrange
    def uuid = dbCreatePerson("John", "Smith")

    // Act
    PersonRepositorySqlImpl repository = new PersonRepositorySqlImpl(sql)
    Person sut = repository.findOneByFirstNameAndLastName("John", "Smith")

    // Assert
    assertThat sut, notNullValue()
    assertThat sut.uuid, is(uuid)
    assertThat sut.name(), is("John Smith")
  }

  @Test
  void canFindPersonByUUID() {

    // Arrange
    def uuid = dbCreatePerson("John", "Smith")

    // Act
    PersonRepositorySqlImpl repository = new PersonRepositorySqlImpl(sql)
    Person sut = repository.findByUUID(uuid)

    // Assert
    assertThat sut, notNullValue()
    assertThat sut.uuid, is(uuid)
    assertThat sut.name(), is("John Smith")
  }

  @Test
  void canNotFindInexistentPersonByName() {

    // Arrange
    def uuid = dbCreatePerson("John", "Smith")

    // Act
    PersonRepositorySqlImpl repository = new PersonRepositorySqlImpl(sql)
    Person sut = repository.findOneByFirstNameAndLastName("Mary", "Smith")

    // Assert
    assertThat sut, nullValue()
  }

  @Test
  void canSavePerson() {

    // Arrange
    Person person = new Person("John", "Smith")

    // Act
    PersonRepositorySqlImpl sut = new PersonRepositorySqlImpl(sql)
    sut.add(person)

    Person savedPerson = sut.findOneByFirstNameAndLastName("John", "Smith")

    // Assert
    assertThat sut, notNullValue()
    assertThat savedPerson.uuid, notNullValue()
    assertThat savedPerson.name(), is("John Smith")
  }

  @Test
  void canSavePersonWithEmail() {

    // Arrange
    def contactMechanismTypeVocabulary = new Vocabulary("CONTACT_MECHANISM_TYPE")
    def emailTerm = new Term("EMAIL_TYPE", contactMechanismTypeVocabulary)
    def personalTerm = new Term("PERSONAL", emailTerm)
    def workTerm = new Term("WORK", emailTerm)
    Email email1 = new Email("john.smith@mail.com", personalTerm)
    Email email2 = new Email("john.smith@softtek.com", workTerm)

    def contactMechanismPurposeTypeVocabulary = new Vocabulary("CONTACT_MECHANISM_PURPOSE_TYPE")
    def emailPurposeTerm = new Term("EMAIL_PURPOSE_TYPE", contactMechanismTypeVocabulary)
    def generalEmailTerm = new Term("PERSONAL_EMAIL", emailPurposeTerm)
    def workEmailTerm = new Term("WORK_EMAIL", emailPurposeTerm)

    Person person = new Person("John", "Smith")
    person.withContactMechanism(email1, generalEmailTerm)
    person.withContactMechanism(email2, workEmailTerm)

    def vocabularyTable = sql.dataSet("vocabulary")
    def termTable = sql.dataSet("term")

    def uuid_vocabulary1 = randomUUID() as String
    vocabularyTable.add( uuid: uuid_vocabulary1,
                         code: contactMechanismTypeVocabulary.code,
                         restricted: 1 )

    def uuid_term1 = randomUUID() as String
    termTable.add( uuid: uuid_term1,
                   code: emailTerm.code,
                   vocabulary_uuid: uuid_vocabulary1,
                   restricted: 1 )

    def uuid_term2 = randomUUID() as String
    termTable.add( uuid: uuid_term2,
                   code: personalTerm.code,
                   vocabulary_uuid: uuid_vocabulary1,
                   parent_uuid: uuid_term1,
                   restricted: 1 )

    def uuid_term3 = randomUUID() as String
    termTable.add( uuid: uuid_term3,
                   code: workTerm.code,
                   vocabulary_uuid: uuid_vocabulary1,
                   parent_uuid: uuid_term2,
                   restricted: 1 )

    def uuid_vocabulary2 = randomUUID() as String
    vocabularyTable.add( uuid: uuid_vocabulary2,
                         code: contactMechanismPurposeTypeVocabulary.code,
                         restricted: 1 )

    def uuid_term4 = randomUUID() as String
    termTable.add( uuid: uuid_term4,
                   code: emailPurposeTerm.code,
                   vocabulary_uuid: uuid_vocabulary1,
                   restricted: 1 )

    def uuid_term5 = randomUUID() as String
    termTable.add( uuid: uuid_term5,
                   code: generalEmailTerm.code,
                   vocabulary_uuid: uuid_vocabulary1,
                   parent_uuid: uuid_term1,
                   restricted: 1 )

    def uuid_term6 = randomUUID() as String
    termTable.add( uuid: uuid_term6,
                   code: workEmailTerm.code,
                   vocabulary_uuid: uuid_vocabulary1,
                   parent_uuid: uuid_term2,
                   restricted: 1 )

    // Act
    PersonRepositorySqlImpl sut = new PersonRepositorySqlImpl(sql)
    sut.add(person)

    Person savedPerson = sut.findOneByFirstNameAndLastName("John", "Smith")

    // Assert
    assertThat sut, notNullValue()
    assertThat savedPerson.uuid, notNullValue()
    assertThat savedPerson.name(), is("John Smith")    
  }

  def dbCreatePerson(String firstName, String lastName) {
    def uuid = randomUUID() as String
    personTable.add( uuid: uuid,
                     first_name: firstName,
                     last_name: lastName
                    )

    return uuid
  }

}
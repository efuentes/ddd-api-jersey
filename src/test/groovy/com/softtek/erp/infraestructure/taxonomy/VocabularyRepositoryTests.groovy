package com.softtek.erp.infraestructure.taxonomy

import static org.junit.Assert.*
import static org.hamcrest.CoreMatchers.*
import org.junit.*

import org.flywaydb.core.Flyway

import static java.util.UUID.randomUUID
import groovy.sql.Sql

import com.softtek.erp.domain.taxonomy.Vocabulary
import com.softtek.erp.domain.taxonomy.Term

class TaxonomyRepositoryTests {

  static Flyway flyway
  static def sql

  static def vocabularyTable, termTable

  @BeforeClass
  static void setup() {
    def db = [url:'jdbc:mysql://127.0.0.1:3306/erp-dev', user:'admin', password:'toor', driver:'com.mysql.jdbc.Driver']
    //def db = [url:'jdbc:mysql://127.0.0.1:49162/erp-test', user:'admin', password:'toor', driver:'com.mysql.jdbc.Driver']
    sql = Sql.newInstance(db.url, db.user, db.password, db.driver)

    flyway = new Flyway()
    flyway.setDataSource(db.url, db.user, db.password)

    vocabularyTable = sql.dataSet("vocabulary")
    termTable = sql.dataSet("term")
  }

  @AfterClass
  static void teardown() {
    sql.close()
  }

  @Before
  void before() {
    flyway.clean()
    flyway.migrate()
  }

  @After
  void after() {
  }

  @Test
  void canFindVocabularyByUUID() {

    // Arrange
    def uuid_vocabulary = dbCreateVocabulary("ORGANIZATION_TYPE", "Organization Type", "Types of Organization.")

    // Act
    VocabularyRepositorySqlImpl repository = new VocabularyRepositorySqlImpl(sql)
    Vocabulary sut = repository.findByUUID(uuid_vocabulary)

    // Assert
    assertThat sut, notNullValue()
    assertThat sut.uuid,notNullValue()
    assertThat sut.code, is("ORGANIZATION_TYPE")
    assertThat sut.name, is("Organization Type")
    assertThat sut.description, is("Types of Organization.")
    assertThat sut.restricted, is(true)
  }

  @Test
  void canAddVocabulary() {

    // Arrange
    Vocabulary vocabulary = new Vocabulary("ORGANIZATION_TYPE").withName("Organization Type").withDescription("Types of Organization.")

    // Act

  }

  def dbCreateVocabulary(String code, String name, String description) {
    def uuid = randomUUID() as String
    vocabularyTable.add( uuid: uuid,
                         code: code,
                         name: name,
                         description: description,
                         restricted: 1)
  }
}
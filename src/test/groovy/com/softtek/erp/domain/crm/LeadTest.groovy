package com.softtek.erp.domain.crm

import com.softtek.erp.domain.taxonomy.Vocabulary
import com.softtek.erp.domain.taxonomy.Term
import com.softtek.erp.domain.party.Person
import com.softtek.erp.domain.party.Organization
import com.softtek.erp.domain.party.Email
import com.softtek.erp.domain.party.GeographicBoundary
import com.softtek.erp.domain.party.PostalAddress
import com.softtek.erp.domain.party.PartyRelationship
import com.softtek.erp.domain.crm.Lead

import static org.junit.Assert.*
import static org.hamcrest.CoreMatchers.*
import org.junit.*

class LeadTest {

  @Test
  void canCreateLeadWithEmail() {

    // Arrange
    Vocabulary geographicBoundaryVocabulary = new Vocabulary("GEOGRAPHIC_BOUNDARY")
    Term countryTerm = new Term("COUNTRY", geographicBoundaryVocabulary).withName("Country")
    GeographicBoundary geoCountryMex = new GeographicBoundary("MX", countryTerm).withName("México").withAbbreviation("Mex")
    Term stateTerm = new Term("STATE", geographicBoundaryVocabulary).withName("State")
    GeographicBoundary geoStateMexDF = new GeographicBoundary("MX-DIF", stateTerm).withParent(geoCountryMex)
    geoStateMexDF.withName("Distrito Federal").withAbbreviation("D.F.")
    PostalAddress postalAddress1 = new PostalAddress("Address 1.1", geoStateMexDF).withAddress2("Address 1.2")

    // Act
    Lead.Builder builder = new Lead.Builder("John", "Smith")
      .withPersonalEmail("john.smith@mail.com") //TODO: Avoid duplication
      .withPostalAddress(postalAddress1) //TODO: Avoid duplication
      .withOrganization("ACME, Co.")

    Lead sut = builder.build()

    // Assert
    assertThat sut, notNullValue()
    assertThat sut.person.name(), is("John Smith")
    assertThat sut.person.partyRoles.size(), is(2)
    assertThat sut.person.partyRoles[0].type.code, is("LEAD")
    assertThat sut.person.contactMechanisms.size(), is(1)
    assertThat sut.person.contactMechanisms[0].email.address, is("john.smith@mail.com")
    assertThat sut.person.contactMechanisms[0].email.type.code, is("PERSONAL")
    assertThat sut.person.contactMechanisms[0].purpose.code, is("PERSONAL_EMAIL")
    assertThat sut.person.postalAddresses.size(), is(1)
    assertThat sut.person.postalAddresses[0].address1, is("Address 1.1")
    assertThat sut.person.postalAddresses[0].address2, is("Address 1.2")
    assertThat sut.person.postalAddresses[0].geographicBoundary.code, is("MX-DIF")
    assertThat sut.person.postalAddresses[0].geographicBoundary.parent.code, is("MX")
    assertThat sut.partyRelationship.from, equalTo(sut.company.partyRoles[0])
    assertThat sut.partyRelationship.to, equalTo(sut.person.partyRoles[0])
    assertThat sut.partyRelationship.type.code, is("EMPLOYMENT")
  }
}
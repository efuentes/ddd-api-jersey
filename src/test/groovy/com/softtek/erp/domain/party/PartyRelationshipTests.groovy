package com.softtek.erp.domain.party

import static org.junit.Assert.*
import static org.hamcrest.CoreMatchers.*
import org.junit.*

import com.softtek.erp.domain.taxonomy.Vocabulary
import com.softtek.erp.domain.taxonomy.Term

class PartyRelationshipTests {

  @Test
  void canCreatePartyRelationshipBetweenPersonAndOrganization() {

    // Arrange
    Vocabulary partyRoleVocabulary = new Vocabulary("PARTY_ROLE")
    
    Term personRoleTerm = new Term("PERSON_ROLE", partyRoleVocabulary)
    Term employeeRoleTerm = new Term("EMPLOYEE", personRoleTerm)
    Person employee = new Person("John", "Smith").withPartyRole(employeeRoleTerm)

    Term organizationRoleTerm = new Term("ORGANIZATION_ROLE", partyRoleVocabulary)
    Term organizationUnitRoleTerm = new Term("INTERNAL_ORGANIZATION", organizationRoleTerm)
    Organization internalOrganization = new Organization("Goodyear").withPartyRole(organizationUnitRoleTerm)

    Vocabulary partyRelationshipVocabulary = new Vocabulary("PARTY_RELATIONSHIP_TYPE")
    Term partyRelationshipTerm = new Term("EMPLOYMENT", partyRelationshipVocabulary)

    // Act
    PartyRelationship sut = new PartyRelationship(internalOrganization.partyRoles[0], employee.partyRoles[0], partyRelationshipTerm)

    // Assert
    assertThat sut, notNullValue()
    assertThat employee.partyRoles.size(), is(1)
    assertThat internalOrganization.partyRoles.size(), is(1)
    assertThat sut.from, equalTo(internalOrganization.partyRoles[0])
    assertThat sut.to, equalTo(employee.partyRoles[0])
    assertThat sut.type, equalTo(partyRelationshipTerm)
  }

}
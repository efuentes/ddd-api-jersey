package com.softtek.erp.application.api.party

import com.softtek.erp.application.api.App
import com.softtek.erp.domain.party.Person

import javax.ws.rs.client.Client
import javax.ws.rs.client.ClientBuilder
import javax.ws.rs.client.WebTarget
import javax.ws.rs.core.Response
import javax.ws.rs.core.MediaType

import org.glassfish.grizzly.http.server.HttpServer

import static org.junit.Assert.*
import static org.hamcrest.CoreMatchers.*
import org.junit.*

class PersonRouteTest {

  private HttpServer server
  private WebTarget target

  @Before
  void setUp() throws Exception {

    server = App.startServer()
    Client c = ClientBuilder.newClient()
    target = c.target(App.BASE_URI)
  }

  @After
  void tearDown() throws Exception {
    server.stop()
  }

  @Test
  void canGetAllPersonResources() {
    //final Response response = target("groovy").request().get()
    //assertThat response.readEntity(String.class), is("groovy")
    ArrayList<Person> response = target.path("persons").request(MediaType.APPLICATION_JSON).get(ArrayList.class)
    //assertThat response.getStatus(), is(200)
    //assertThat response, notNullValue()
    assertThat response.size(), is(1)
    assertThat response[0].firstName, is("John")
    assertThat response[0].lastName, is("Smith")
  }
}
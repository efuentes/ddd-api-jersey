package com.softtek.erp.application.api.party

import com.softtek.erp.application.api.App

import javax.ws.rs.client.Client
import javax.ws.rs.client.ClientBuilder
import javax.ws.rs.client.WebTarget
//import javax.ws.rs.core.Response

import org.glassfish.grizzly.http.server.HttpServer

import static org.junit.Assert.*
import static org.hamcrest.CoreMatchers.*
import org.junit.*

class GroovyRouteTest {

  private HttpServer server
  private WebTarget target

  @Before
  void setUp() throws Exception {

    server = App.startServer()
    Client c = ClientBuilder.newClient()
    target = c.target(App.BASE_URI)
  }

  @After
  void tearDown() throws Exception {
    server.stop()
  }

  @Test
  void testGroovyResource() {
    //final Response response = target("groovy").request().get()
    //assertThat response.readEntity(String.class), is("groovy")
    String responseMsg = target.path("groovy").request().get(String.class)
    assertThat responseMsg, is("groovy")
  }
}